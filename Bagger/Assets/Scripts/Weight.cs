﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Weight : MonoBehaviour {

    public float wps = 1.0f;
    public ProcessCollisions[] ColliderList;
	// Use this for initialization
	void Start () {
        ColliderList = FindObjectsOfType<ProcessCollisions>();
	}
	
	// Update is called once per frame
	void Update () {
        float weight = 0;
        for (int i = 0; i < ColliderList[0].colls.Count; i++)
        {
            bool existsall = true;
            for (int j = 1; j < ColliderList.Length; j++)
            {
                if (!ColliderList[j].colls.Contains(ColliderList[0].colls[i]))
                {
                    existsall = false;
                    goto skip;
                }
            }
            skip:
            if (existsall) weight += wps;
        }
        GetComponent<Rigidbody>().mass = weight;
	}
}
