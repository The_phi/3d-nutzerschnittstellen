﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MeassureAngle : MonoBehaviour {

    public float m_fAngleMargin = 8;

    private Transform m_transThis;

    public float m_fDeviationScore = 0;

    private bool m_bIsHorizontal = true;

    public Text m_balanceText;

    private float m_fhorizontalStartTime;

	// Use this for initialization
	void Start () {
        m_transThis = transform;

    }
	
	// Update is called once per frame
	void Update () {
        checkRotation();
        setBalanceText();
    }

    private void setBalanceText()
    {
        if(m_bIsHorizontal)
        {
            if((Time.time - m_fhorizontalStartTime) > 10)
            {
                m_balanceText.color = Color.green;
            }
            m_balanceText.text = "Balance is reached";
        } else
        {
            m_balanceText.text = "Not balanced";
            m_balanceText.color = Color.red;
        }
    }

    private void checkRotation()
    {
        m_fDeviationScore = Mathf.Abs(m_transThis.rotation.z);
        m_fDeviationScore = m_fDeviationScore * 360;

        if (m_fDeviationScore <= m_fAngleMargin)
        {
            if(m_bIsHorizontal == false)
            {
                m_bIsHorizontal = true;
                m_fhorizontalStartTime = Time.time;
            }
        }
        else
        {
            m_bIsHorizontal = false;
        }
    }
}
