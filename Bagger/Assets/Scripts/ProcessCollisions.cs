﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ProcessCollisions : MonoBehaviour {

    public ArrayList colls = new ArrayList();
    
	// Use this for initialization
	void Start () {
 
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        colls.Add(other.gameObject);
    }

    void OnTriggerExit(Collider other)
    {
        colls.Remove(other.gameObject);
    }
}
