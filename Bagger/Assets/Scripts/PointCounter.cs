﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PointCounter : MonoBehaviour {

    private int m_iPoints = 0;

    public Text m_textPoints;

	// Use this for initialization
	void Start () {
        updateDisplayedPoints();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void updateDisplayedPoints()
    {
        m_textPoints.text = "Collected Points: " + m_iPoints;
    }

    private void OnTriggerEnter(Collider _collider)
    {
        if(_collider.gameObject.CompareTag("Sphere"))
        {
            m_iPoints++;
            updateDisplayedPoints();
        }
    }

    private void OnTriggerExit(Collider _collider)
    {
        if(_collider.gameObject.CompareTag("Sphere"))
        {
            m_iPoints--;
            updateDisplayedPoints();
        }
    }
}
