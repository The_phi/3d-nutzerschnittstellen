﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

/*
 * Handles the vuforia movement.
 * Also handles keyboard controls.
 */
public class CopyVuforiaTracking : MonoBehaviour, ITrackableEventHandler
{

    public Rigidbody m_rbOther;
    public Rigidbody m_rbGhost;
    public Rigidbody m_rbCamera;
    public Vector3 scaling = new Vector3(1f, 1f, 1.0f);

    private Transform m_transThis;

    private Vector3 m_v3OtherOrigPos;

    private Vector3 m_v3ThisOrigPos;

    private Vector3 m_v3OrigCameraPosition;
    private Quaternion m_quatOrigCameraRotation;
    private Quaternion m_quatCurrCameraRotation;
    private Quaternion m_quatOrigOtherRotation;
    private Quaternion m_quatCurOtherRotation;

    public float m_fPosTranslScaleX = -20;
    public float m_fPosTranslScaleY = 30;
    public float m_fPosTranslScaleZ = -20;

    public bool m_bLockZAxisMovement = true;

    public bool m_bUseKeyboard = false;
    private Vector3 m_v3Rotation = new Vector3(-90, 90, 0);
    private float m_fSpeed = 0.05f;

    public bool direct = true;

    public Text m_StatusInfo;
    public Text m_StatusText;

    private State m_trackstate = State.Lost;

    private float m_fDeadzoneSize = 2f;
    private float m_fRotationDeadZone = 20f;

    public float m_bucketRelContrRotScale = 0.004f;
    public float m_cameraRelContrRotScale = 0.004f;
    public float m_bucketRelContMoveScale = 0.01f;
    public float m_cameraRelContMoveScale = 0.004f;

    private enum State
    {
        Lost,
        Ghost,
        Tracking,
        CameraMovement,
        CameraLost
    }

    private TrackableBehaviour m_TrackableBehaviour;

    // Use this for initialization
    void Start()
    {
        m_TrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (m_TrackableBehaviour)
        {
            m_TrackableBehaviour.RegisterTrackableEventHandler(this);
        }


        refreshInfos();
    }

    private void Awake()
    {
        m_transThis = this.transform;
        m_v3ThisOrigPos = m_transThis.position;
        m_v3OtherOrigPos = m_rbOther.position;
        m_v3OrigCameraPosition = m_rbCamera.position;

        m_quatOrigCameraRotation = m_rbCamera.rotation;
        m_quatCurrCameraRotation = m_rbCamera.rotation;
        m_quatOrigOtherRotation = m_rbOther.rotation;
        m_quatCurOtherRotation = m_rbOther.rotation;
        //m_quatOrigOtherRotation.y *= -1;
        //m_quatOrigOtherRotation.w *= -1;
    }

    void Update()
    {


        if (m_bUseKeyboard == false)
        {
            applyMarkerMovementInput();
        }
        else // mouse control
        {
            applyKeyboardInput();
        }
    }

    private void applyMarkerMovementInput()
    {
        Rigidbody target = m_rbOther;

        //set target of movement
        switch (m_trackstate)
        {
            case State.Lost:
                //nothing to do here!
                return;
            case State.Ghost:
                //ghost is update target
                target = m_rbGhost;
                break;
            case State.Tracking:
                //bowl is update target
                target = m_rbOther;
                break;
            case State.CameraMovement:
                //camera is update target
                target = m_rbCamera;
                break;
            default: //case State.CameraLost
                return;
        }

        // fix wrong rotation from blender/ vuforia of MARKER
        Quaternion newRot = m_transThis.rotation * Quaternion.Euler(90, 0, 0);
        newRot.y *= -1;
        newRot.w *= -1;
        Vector3 v3Offset = calculateOffset();


        //update UI
        UpdateUI(v3Offset);

        if (direct) //direct control
        {
            v3Offset.z = 0;
            if (m_trackstate >= State.CameraMovement) //camera
            {
                //m_v3OtherOrigPos = m_v3OtherOrigPos + v3Offset * 5;
                target.MovePosition(m_v3OrigCameraPosition + v3Offset * 5);
                target.MoveRotation(newRot);

                //recalculate OtherOrigPosition with "raycast" -> from center of camera!
                /*Vector3 d = newRot.eulerAngles.normalized;
                Vector3 c = m_v3OrigCameraPosition + v3Offset * 5;

                float a = Vector3.Dot(new Vector3(0, 0, 12.89f) - c, new Vector3(0, 0, 1));
                float b = Vector3.Dot(d, new Vector3(0, 0, 1));

                if (a != 0 && b!= 0)
                {
                    m_v3OtherOrigPos = a / b * d + c;
                    m_rbGhost.position = m_v3OtherOrigPos; // teleport ghost to new offset (Avoids errors from lerping)
                }*/

                Vector3 newGhostPosition = getNewGhostPosition();
                if(newGhostPosition != Vector3.zero)
                {
                    m_v3OtherOrigPos = newGhostPosition;
                }

                Debug.Log(m_v3OtherOrigPos);

            }
            else //other
            {
                target.MovePosition(m_v3OtherOrigPos + v3Offset);
                target.MoveRotation(newRot);
            }
        }
        else // relative control
        {
            caclulateRelativeControl(v3Offset, target, newRot);
        }

        //do other stuff:

        //Check if Ghostmode is over
        if (m_trackstate == State.Ghost)
        {
            if ((m_rbGhost.transform.position - m_rbOther.transform.position).sqrMagnitude <= 0.7f && Vector3.Angle(m_rbGhost.transform.rotation.eulerAngles, m_rbOther.transform.rotation.eulerAngles) <= 8.0f)
            {
                m_trackstate = State.Tracking;
                m_rbGhost.GetComponent<MeshRenderer>().enabled = false;

                refreshInfos();
            }
        }
    }

    private void caclulateRelativeControl(Vector3 v3Offset, Rigidbody target, Quaternion newRotation)
    {
        if (m_trackstate == State.Ghost) //no ghost in relative mode!
        {
            m_trackstate = State.Tracking;
            m_rbGhost.GetComponent<MeshRenderer>().enabled = false;

            refreshInfos();
            return; // skip one frame of movement to retarget
        }

        v3Offset.z = 0; //fix z-Axis

        if (m_trackstate >= State.CameraMovement) //camera
        {
            if (v3Offset.sqrMagnitude >= m_fDeadzoneSize) //Deadzone
            {
                v3Offset = v3Offset * m_cameraRelContMoveScale; //scale movement
                m_v3OrigCameraPosition += v3Offset; //update old positions
                //m_v3OtherOrigPos = m_v3OtherOrigPos + v3Offset;

                target.MovePosition(m_v3OrigCameraPosition);//set camera to new position


                //No recalc of orig neccessary-> its relative u know
            }
            //rotate camera
            if (Mathf.Abs(Quaternion.Angle(m_quatOrigCameraRotation, newRotation)) >= m_fRotationDeadZone) //Deadzone
            {
                m_quatCurrCameraRotation = Quaternion.Slerp(m_quatCurrCameraRotation, m_quatCurrCameraRotation * newRotation, m_cameraRelContrRotScale); //scale
                target.MoveRotation(m_quatCurrCameraRotation); //reposition
            }
        }
        else //other
        {
            if (v3Offset.sqrMagnitude >= m_fDeadzoneSize) //Deadzone
            {
                v3Offset = v3Offset * m_bucketRelContMoveScale; //scale movement
                m_v3OtherOrigPos += v3Offset;

                target.MovePosition(m_v3OtherOrigPos);//set Target to new position
            }
            //rotate target
            if (Mathf.Abs(Quaternion.Angle(m_quatOrigOtherRotation, newRotation)) >= m_fRotationDeadZone) //Deadzone
            {
                m_quatCurOtherRotation = Quaternion.Slerp(m_quatCurOtherRotation, m_quatCurOtherRotation * newRotation, m_bucketRelContrRotScale);
                target.MoveRotation(m_quatCurOtherRotation); //reposition
            }
        }
    }

    // calculates the current offset from the original position of the multi target
    private Vector3 calculateOffset()
    {
        Vector3 v3Offset = m_transThis.position - m_v3ThisOrigPos;
        v3Offset.x = v3Offset.x * m_fPosTranslScaleX*scaling.x;
        v3Offset.y = v3Offset.y * m_fPosTranslScaleY* scaling.y;
        v3Offset.z = -v3Offset.z * m_fPosTranslScaleZ* scaling.z;
        return v3Offset;
    }

    private void applyKeyboardInput()
    {
        float fMoveVertical = Input.GetAxis("Vertical");
        float fMoveHorizontal = Input.GetAxis("Horizontal");

        Vector3 v3Velocity = new Vector3(fMoveHorizontal, fMoveVertical, 0.0f);
        m_v3OtherOrigPos = m_v3OtherOrigPos + v3Velocity * m_fSpeed;
        m_rbOther.MovePosition(m_v3OtherOrigPos);

        if (Input.GetKey(KeyCode.Q))
        {
            m_v3Rotation.x = m_v3Rotation.x - 1;
        }
        if (Input.GetKey(KeyCode.E))
        {
            m_v3Rotation.x = m_v3Rotation.x + 1;
        }
        m_rbOther.MoveRotation(Quaternion.Euler(m_v3Rotation));
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        Debug.Log(previousStatus.ToString() + " " + newStatus.ToString());

        if(m_trackstate >= State.CameraMovement)
        {
            if (newStatus == TrackableBehaviour.Status.TRACKED)
            {
                m_trackstate = State.CameraMovement;
            } else
            {
                m_trackstate = State.CameraLost;
            }
            refreshInfos();
            return;
        }

        if (newStatus == TrackableBehaviour.Status.TRACKED)
        {
            m_trackstate = State.Ghost;
            m_rbGhost.GetComponent<MeshRenderer>().enabled = true;
        } else
        {
            m_trackstate = State.Lost;
            m_rbGhost.GetComponent<MeshRenderer>().enabled = false;
        }

        refreshInfos();
    }

    public void switchCameraMovement()
    {
        if(m_trackstate >= State.CameraMovement)
        {
            m_trackstate = State.Ghost;

            Vector3 diff = m_v3OrigCameraPosition - m_rbCamera.position;
            diff.z = 0;

            m_v3OtherOrigPos = m_v3OtherOrigPos + diff;
            m_rbGhost.GetComponent<MeshRenderer>().enabled = true;
        } else
        {
            if(m_trackstate == State.Lost || m_trackstate == State.CameraLost)
            {
                m_trackstate = State.CameraLost;
            } else {
                m_trackstate = State.CameraMovement;
            }
        }


        refreshInfos();
    }

    public void OnValueChangedX(Slider slider)
    {
        scaling.x = slider.value;
        slider.GetComponentInChildren<Text>().text = slider.value.ToString("0.0");
    }
    public void OnValueChangedY(Slider slider)
    {
        scaling.y = slider.value;
        slider.GetComponentInChildren<Text>().text = slider.value.ToString("0.0");
    }

    public void refreshInfos()
    {
        switch (m_trackstate)
        {
            case State.Lost:
                m_StatusInfo.text = "Tracking Lost!";
                m_StatusText.text = "Lost tracking, make sure to hold the marker in the middle of your camera view."; 
                break;
            case State.Ghost:
                m_StatusInfo.text = "Ghostmode!";
                m_StatusText.text = "Lost tracking, try to aling the ghost to the bowl to regain physical control. You may have to move the Camera.";
                break;
            case State.Tracking:
                if (direct)
                {
                    m_StatusInfo.text = "Direct tracking!";
                    m_StatusText.text = "Everyting is fine. Move as if the marker is the bowl.";
                } else
                {
                    m_StatusInfo.text = "Relative tracking!";
                    m_StatusText.text = "Everyting is fine. Move outside the Deadzone as if you want to show a direction with the marker to the bowl";
                }
                break;
            case State.CameraMovement:
                if (direct)
                {
                    m_StatusInfo.text = "Direct camera control!";
                    m_StatusText.text = "Camera Tracking Mode. Move as if the marker is the Camera.";
                } else {

                    m_StatusInfo.text = "Relative camera control!";
                    m_StatusText.text = "Camera Tracking Mode. Move outside the Deadzone as if you want to show a direction with the marker to the camera";
                }
                break;
            case State.CameraLost:
                m_StatusInfo.text = "Camera Lost!";
                m_StatusText.text = "Lost tracking, recenter the marker to regain control.";
                break;
            default:
                m_StatusInfo.text = "NO MODE!";
                m_StatusText.text = "NO IDEA WHAT HAPPENED";
                break;
        }
    }

    private void UpdateUI(Vector3 v)
    {
        GameObject TargetMarker = GameObject.Find("TargetMarker");
        TargetMarker.transform.localPosition = new Vector3(v.x / 1f * 50, v.y / 1f * 50, 0);

    }

    private Vector3 getNewGhostPosition()
    {
        Vector3 v3CamPos = m_rbCamera.position;
        Quaternion quatCamRot = m_rbCamera.rotation;

        int layer_mask = LayerMask.GetMask("GhostPositioningPlane");
        float distance = 50f;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, distance, layer_mask))
        {
            Vector3 hitPoint = hit.point;
            return hitPoint;
        }

        return Vector3.zero;
    }
}
