﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *  allows to drag an object around with the mouse
 */
public class MouseMove : MonoBehaviour {

    private Vector3 distance;


    void OnMouseDown()
    {
        distance = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0)) - transform.position;
    }

    void OnMouseDrag()
    {
        Vector3 distance_to_screen = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        Vector3 pos_move = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance_to_screen.z));
        transform.position = new Vector3(pos_move.x - distance.x, transform.position.y, pos_move.z);

    }
}
