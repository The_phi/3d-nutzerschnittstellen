﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyRotTrans : MonoBehaviour {

    public GameObject target;
    public bool initialOffset = true;
    public Vector3 offset;
	// Use this for initialization
	void Start () {
        if (initialOffset) offset = transform.position-target.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = offset+target.transform.position;
        transform.rotation = target.transform.rotation;
	}
}
